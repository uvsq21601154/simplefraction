public class Fraction {
    int numerateur;
    int denominateur;

    public Fraction(){
        setNumerateur(1);
        setDenominateur(2);
    }

    public Fraction(int numerateur, int denominateur){
        setNumerateur(numerateur);
        setDenominateur(denominateur);
    }
    public String toString() {
        return numerateur + "/" + denominateur;
    }

    public int getNumerateur() {
        return numerateur;
    }

    public void setNumerateur(int numerateur) {
        this.numerateur = numerateur;
    }

    public int getDenominateur() {
        return denominateur;
    }

    public void setDenominateur(int denominateur) {
        this.denominateur = denominateur;
    }
}
